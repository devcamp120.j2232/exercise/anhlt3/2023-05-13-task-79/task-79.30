package com.devcamp.exportexcel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.exportexcel.model.CarModel;

public interface ICarModelRepository extends JpaRepository<CarModel, Long> {
	CarModel findByCarCode(String carCode);
}
