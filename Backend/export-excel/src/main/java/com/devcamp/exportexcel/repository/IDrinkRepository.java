package com.devcamp.exportexcel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.exportexcel.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {

}
